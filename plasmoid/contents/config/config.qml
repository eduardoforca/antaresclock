import QtQuick 2.2
import org.kde.plasma.configuration 2.0

ConfigModel {
    ConfigCategory {
         name: i18n('Appearence')
         icon: 'preferences-desktop-color'
         source: 'config/ConfigAppearence.qml'
    }
    ConfigCategory {
         name: i18n('Weather')
         icon: 'weather-clear'
         source: 'config/ConfigWeather.qml'
    }
}
