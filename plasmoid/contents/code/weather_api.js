/*
 * Copyright 2021  Eduardo Força
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http: //www.gnu.org/licenses/>.
 */

/**
 * Pull the most recent observation from the selected weather station.
 *
 * This handles setting errors and making the loading screen appear.
 */
function getCurrentData() {
	var req = new XMLHttpRequest();

	var url = "https://api.openweathermap.org/data/2.5/weather";
	if (!!cityName) {
		url += `?q=${cityName}`;
		if (countryCode) {
			url += `,${countryCode}`;
		}
	} else {
		url += `?lat=${locLat}&lon=${locLon}`;;
	}
	url += `&appid=${apiKey}`;
	url += `&lang=${lang}`;

	req.open("GET", url);

	req.setRequestHeader("Accept-Encoding", "gzip");

	req.onreadystatechange = function () {
		if (req.readyState == 4) {
			if (req.status == 200) {
				data = JSON.parse(req.responseText);
			}
		}
	};

	req.send();
}