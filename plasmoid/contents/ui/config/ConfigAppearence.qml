/*
 * Copyright 2021  Eduardo Força <edusousa1996@hotmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http: //www.gnu.org/licenses/>.
 */
import QtQuick 2.2
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

Item {

    QtObject {
        id: backgroundValue
        property var value
    }
    
    property alias cfg_font1: font1Dialog.font
    property alias cfg_font2: font2Dialog.font
    property alias cfg_font3: font3Dialog.font
    property alias cfg_font1Size: font1SizeSB.value
    property alias cfg_font2Size: font2SizeSB.value
    property alias cfg_font3Size: font3SizeSB.value
    property alias cfg_useSystemColorForText: useSystemColorForText.checked
    property alias cfg_textColor: textColorPicker.chosenColor
    property alias cfg_useSystemHG: useSystemColorHG.checked
    property alias cfg_hgColor: hgColorPicker.chosenColor
    property alias cfg_dateFormat: dateFormat.text
    property alias cfg_timeFormat: timeFormat.text
    property alias cfg_background: backgroundValue.value
    property alias cfg_iconHasColor: iconHasColor.checked
    property alias cfg_iconColor: iconColorPicker.chosenColor
    
    GridLayout {
        columns: 2
        Layout.fillWidth: true;

        Label {
            text: i18n("Background:")
        }
        ComboBox {
            textRole: "text"
            valueRole: "value"
			id: backgroundComboBox
			model: [
                {text: i18n("None"), value: 0},
                {text: i18n("Standard"), value: 1},
                {text: i18n("Translucent"), value: 2},
                {text: i18n("Shadowed"), value: 4}
            ]
            onActivated: backgroundValue.value = currentValue
            Component.onCompleted: currentIndex = indexOfValue(backgroundValue.value)
		}


        Label {
            Layout.columnSpan: 2
            Layout.topMargin: 20
            text: i18n('Main Font:')
            font.underline: true
            font.weight: Font.Bold
        }
        RowLayout {
            Layout.columnSpan: 2   
            RowLayout {
                
                Label {
                    text: font1Dialog.font.family
                    Layout.fillWidth: true
                    wrapMode: Text.Wrap
                }
                Button {
                    text: i18n("Choose font")
                    onClicked: font1Dialog.visible = true;
                    FontDialog {
                        id: font1Dialog
                    }
                }
            }
        }

        RowLayout {
            Layout.columnSpan: 2
            Label {
                text: i18n("Font size:")
            }
            SpinBox {
                id: font1SizeSB
                from: 0
                to: 100 * 100
                stepSize: 1

                validator: DoubleValidator {
                    bottom: Math.min(font1SizeSB.from, font1SizeSB.to)
                    top:  Math.max(font1SizeSB.from, font1SizeSB.to)
                }

                textFromValue: function(value, locale) {
                    return value.toString() + "%"
                }

                valueFromText: function(text, locale) {
                    return text.match(/(\d)*/g)[0]
                }
            }
        }


        Label {
            Layout.columnSpan: 2
            Layout.topMargin: 20
            text: i18n('Location Font:')
            font.underline: true
            font.weight: Font.Bold
        }
        RowLayout {
            Layout.columnSpan: 2
            RowLayout {
                
                Label {
                    text: font2Dialog.font.family
                    Layout.fillWidth: true
                    wrapMode: Text.Wrap
                }
                Button {
                    text: i18n("Choose font")
                    onClicked: font2Dialog.visible = true;
                    FontDialog {
                        id: font2Dialog
                    }
                }
            }
        }

        RowLayout {
            Layout.columnSpan: 2
            Label {
                text: i18n("Font size:")
            }
            SpinBox {
                id: font2SizeSB
                from: 0
                to: 100 * 100
                stepSize: 1

                validator: DoubleValidator {
                    bottom: Math.min(font2SizeSB.from, font2SizeSB.to)
                    top:  Math.max(font2SizeSB.from, font2SizeSB.to)
                }

                textFromValue: function(value, locale) {
                    return Math.floor(value).toString() + "%"
                }

                valueFromText: function(text, locale) {
                    return text.match(/(\d)*/g)[0]
                }
            }
        }

        Label {
            Layout.columnSpan: 2
            Layout.topMargin: 20
            text: i18n('Clock Font:')
            font.underline: true
            font.weight: Font.Bold
        }
        RowLayout {
            Layout.columnSpan: 2
            RowLayout {
                
                Label {
                    text: font3Dialog.font.family
                    Layout.fillWidth: true
                    wrapMode: Text.Wrap
                }
                Button {
                    text: i18n("Choose font")
                    onClicked: font3Dialog.visible = true;
                    FontDialog {
                        id: font3Dialog
                    }
                }
            }
        }

        RowLayout {
            Layout.columnSpan: 2
            Label {
                text: i18n("Font size:")
            }
            SpinBox {
                id: font3SizeSB
                from: 0
                to: 100 * 100
                stepSize: 1

                validator: DoubleValidator {
                    bottom: Math.min(font3SizeSB.from, font3SizeSB.to)
                    top:  Math.max(font3SizeSB.from, font3SizeSB.to)
                }

                textFromValue: function(value, locale) {
                    return value.toString() + "%"
                }

                valueFromText: function(text, locale) {
                    return text.match(/(\d)*/g)[0]
                }
            }
        }

        Label {
            Layout.columnSpan: 2
            Layout.topMargin: 20
            text: i18n('Colors:')
            font.underline: true
            font.weight: Font.Bold
        }
        
        CheckBox {
            id: useSystemColorForText
            text: i18n('Use system color for text')
            Layout.columnSpan: 2
        }
        
        Label {
            enabled: !useSystemColorForText.checked
            text: i18n("Text Color:")
        }
    
        ColorPicker{
            id: textColorPicker
            enabled: !useSystemColorForText.checked
        }

        CheckBox {
            id: useSystemColorHG
            text: i18n('Use system color for highlights')
            Layout.columnSpan: 2
        }
        
        Label {
            text: i18n("Highlight Color:")
            enabled: !useSystemColorHG.checked
        }
    
        ColorPicker{
            id: hgColorPicker
            enabled: !useSystemColorHG.checked
        }

        CheckBox {
            id: iconHasColor
            text: i18n('Paint weather icon:')
            Layout.columnSpan: 2
        }
        
        Label {
            text: i18n("Weather Icon Color:")
            enabled: iconHasColor.checked
        }
    
        ColorPicker{
            id: iconColorPicker
            enabled: iconHasColor.checked
        }

        Label {
            Layout.columnSpan: 2
            Layout.topMargin: 20
            text: i18n('Formats:')
            font.underline: true
            font.weight: Font.Bold
        }

        Label {
            text: i18n('Date format:')
        }
            
        TextField {
            id: dateFormat
            width: 200
        }

        Label {
            text: i18n('Time format:')
        }
            
        TextField {
            id: timeFormat
            width: 200
        }
    }
}
