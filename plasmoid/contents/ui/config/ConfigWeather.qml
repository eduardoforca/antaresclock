/*
 * Copyright 2021  Eduardo Força <edusousa1996@hotmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http: //www.gnu.org/licenses/>.
 */
import QtQuick 2.2
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import org.kde.plasma.private.weather 1.0 as WeatherPlugin
import "../libweather"

Item {
    
    property alias cfg_apiKey: apiKey.text
    property alias cfg_updateInterval: updateInterval.value
    property alias cfg_cityName: cityName.text
    property alias cfg_countryCode: countryCode.text
    property alias cfg_apiLang: apiLang.text
    
    GridLayout {
        columns: 2
        Layout.fillWidth: true;

        Label {
            Layout.columnSpan: 2
            text: 'Go to <a href="https://openweathermap.org/">OpenWeather</a> to get an API key'
        }

        Label {
            text: i18n('OpenWeather API Key:')
        }
        TextField {
            id: apiKey
            Layout.fillWidth: true
        }

        Label {
            text: i18n('Update interval (seconds):')
        }
        SpinBox {
            id: updateInterval
        }

        Label {
            Layout.columnSpan: 2
            Layout.topMargin: 20
            text: i18n('Location Data (leave blank to use system data)')
            font.underline: true
            font.weight: Font.Bold
        }

        Label {
            text: i18n('City Name:')
        }
        TextField {
            id: cityName
            width: 200
            placeholderText: '(Ex: Atlanta)'
        }

        Label {
            text: i18n('Country Code:')
        }
        TextField {
            id: countryCode
            width: 200
            placeholderText: '(Ex: US)'
        }

        Label {
            text: i18n('API language:')
        }
        TextField {
            id: apiLang
            width: 200
            placeholderText: "(Ex: en)"
        }

        Label {
            Layout.columnSpan: 2
            Layout.topMargin: 20
            text: i18n('Units')
            font.underline: true
            font.weight: Font.Bold
        }

        property alias displayUnits: displayUnits
	    DisplayUnits { id: displayUnits }

        Label {
            text: i18n("Temperature Unit:")
        }
        ConfigUnitComboBox {
			id: temperatureComboBox
			configKey: 'temperatureUnitId'
			model: WeatherPlugin.TemperatureUnitListModel

			function serializeWith(nextValue) {
				displayUnits.setTemperatureUnitId(nextValue)
			}
			Component.onCompleted: {
				temperatureComboBox.populateWith(displayUnits.temperatureUnitId)
			}
		}
        
        Label {
            text: i18n("Speed Unit:")
        }
        ConfigUnitComboBox {
			id: windSpeedComboBox
			configKey: 'windSpeedUnitId'
			model: WeatherPlugin.WindSpeedUnitListModel

			function serializeWith(nextValue) {
				displayUnits.setWindSpeedUnitId(nextValue)
			}
			Component.onCompleted: {
				windSpeedComboBox.populateWith(displayUnits.windSpeedUnitId)
			}
		}
    }
}
