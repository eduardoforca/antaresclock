/*
 * Copyright 2021  Eduardo Força <edusousa1996@hotmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http: //www.gnu.org/licenses/>.
 */
import QtQuick 2.2
import QtQuick.Layouts 1.3
import org.kde.plasma.plasmoid 2.0
import QtQml 2.0

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.private.weather 1.0 as WeatherPlugin
import QtGraphicalEffects 1.0
import "./libweather"

Item {
    property font font1: plasmoid.configuration.font1
    property font font2: plasmoid.configuration.font2
    property font font3: plasmoid.configuration.font3
    property bool useSystemColorForText: plasmoid.configuration.useSystemColorForText
    property string textColor: useSystemColorForText ? PlasmaCore.ColorScope.textColor : plasmoid.configuration.textColor || "#ffffff"
    property bool useSystemHG: plasmoid.configuration.useSystemHG
    property string hgColor:  useSystemHG ? PlasmaCore.ColorScope.highlightColor : plasmoid.configuration.hgColor || "#41b4c7"
    property string dateFormat:  plasmoid.configuration.dateFormat

    property var larger: (width * 0.7) < height ? (width * 0.7): height

    property var fontSize: (larger / 16) || 1

    id: root

    WeatherData {
		id: weatherData
	}

    PlasmaCore.DataSource {
        id: dataSource
        engine: "time"
        connectedSources: ["Local"]
        interval: 500
    }
    Layout.preferredWidth: 360 * units.devicePixelRatio
    width: Layout.preferredWidth
    height: width * 0.7

    // Layout.minimumWidth: height / 0.7
    // Layout.minimumHeight: width * 0.7

    // Plasmoid.backgroundHints: "NoBackground";
    Plasmoid.backgroundHints: plasmoid.configuration.background || PlasmaCore.Types.NoBackground
    // PlasmaCore.Types.NoBackground | PlasmaCore.Types.ConfigurableBackground
    // Plasmoid.compactRepresentation: {}
    // Plasmoid.fullRepresentation: {}
    ColumnLayout {
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        spacing: 0
        PlasmaComponents.Label {
            id: weekday
            color: textColor
            font.family: font1
            font.capitalization: Font.AllLowercase
            font.pointSize: fontSize * 3 * (plasmoid.configuration.font1Size / 100);
            text: `<font color="${hgColor}">.</font>${Qt.formatDate(dataSource.data.Local.DateTime, "dddd").split("-")[0]}`
            renderType: Text.QtRendering
            font.weight: Font.Black
        }
        PlasmaComponents.Label {
            visible: dateFormat
            Layout.topMargin: -1 * fontSize
            id: date
            color: textColor
            font.family: font1
            font.pointSize: fontSize * 1.2 * (plasmoid.configuration.font1Size / 100);
            font.weight: Font.Medium
            text: Qt.formatDate(dataSource.data.Local.DateTime, dateFormat)
            renderType: Text.QtRendering
        }
        RowLayout {
            visible: weatherData.hasData
            Item {
                id: weatherIconBox
                Layout.preferredWidth: fontSize * 3
                Layout.preferredHeight: fontSize * 3
                Rectangle {
                    opacity: plasmoid.configuration.iconHasColor ? 0.8 : 1
                    id: iconRect
                    color: hgColor
                    anchors.fill: parent
                    anchors.centerIn: parent
                    visible: !!plasmoid.configuration.iconHasColor
                }
                Rectangle {
                    id: weatherIcon
                    color: "transparent"
                    anchors.fill: parent
                    anchors.centerIn: parent
                    smooth: true
                    visible: !!plasmoid.configuration.iconHasColor
                    PlasmaCore.SvgItem {
                        id: weatherSvg
                        anchors.fill: parent
                        anchors.centerIn: parent
                        anchors.margins: fontSize * 0.2
                        svg: PlasmaCore.Svg {imagePath: plasmoid.file("", "icons/" + weatherData.currentConditionIconName + ".svg" ||
                                                                         "icons/null.svg")}
                    }
                    ColorOverlay {
                        cached: false
                        id: weatherSvgColor
                        anchors.fill: weatherSvg
                        source: weatherSvg
                        color: plasmoid.configuration.iconColor
                        visible: plasmoid.configuration.iconHasColor
                    }
                }
                OpacityMask {
                    opacity: plasmoid.configuration.iconHasColor ? 1 : 0.8
                    visible: !plasmoid.configuration.iconHasColor
                    id: opmsk
                    anchors.fill: iconRect
                    source:iconRect
                    maskSource: weatherIcon
                    invert: true
                }
            }
            ColumnLayout {
                spacing: -0.5 * fontSize
                PlasmaComponents.Label {
                    // Layout.alignment: Qt.AlignBottom
                    // verticalAlignment: Text.AlignBottom
                    id: conditionLabel
                    color: textColor
                    font.family: font1
                    font.pointSize: fontSize * 0.85 * (plasmoid.configuration.font1Size / 100);
                    font.capitalization: Font.Capitalize
                    text: `${weatherData.currentConditionsLong}`
                    renderType: Text.QtRendering
                    font.weight: Font.Medium
                }
                RowLayout {
                    PlasmaComponents.Label {
                        Layout.alignment: Qt.AlignTop
                        // verticalAlignment: Text.AlignVCenter
                        id: currentTempLabel
                        readonly property var temp: weatherData.currentTemp
                        color: textColor
                        font.family: font1
                        font.pointSize: fontSize * 0.85 * (plasmoid.configuration.font1Size / 100);
                        font.capitalization: Font.Capitalize
                        text: `<font size="6"> ${weatherData.formatTemp(temp, true, false)}</font>`
                        renderType: Text.QtRendering
                        font.weight: Font.Medium
                    }
                    ColumnLayout {
                        spacing: -0.3 * fontSize
                        // Layout.alignment: Qt.AlignTop
                        // Layout.alignment: Qt.AlignBottom
                        PlasmaComponents.Label {
                            readonly property var max: weatherData.maxTemp
                            readonly property var min: weatherData.minTemp
                            
                            color: textColor
                            font.family: font1
                            font.pointSize: fontSize * 0.66 * (plasmoid.configuration.font1Size / 100);
                            text: `<b>${weatherData.formatTempShort(max)}</b>|${weatherData.formatTempShort(min)}`
                            renderType: Text.QtRendering
                            font.weight: Font.Medium
                        }
                        RowLayout {
                            Layout.alignment: Qt.AlignLeft
                            visible: weatherData.humidity
                            spacing: 0
                            Item {
                                Layout.preferredWidth: fontSize * 1.2
                                Layout.preferredHeight: fontSize * 1.2
                                Layout.alignment: Qt.AlignVCenter
                                PlasmaCore.SvgItem {
                                    id: humidIcon
                                    visible: false
                                    smooth: false
                                    anchors.fill: parent
                                    svg: PlasmaCore.Svg {imagePath: plasmoid.file("", "icons/" + "drop" + ".svg")}
                                }
                                ColorOverlay {
                                    id: humidIconColor
                                    anchors.fill: humidIcon
                                    source:humidIcon
                                    color: textColor
                                }
                            }
                            PlasmaComponents.Label {
                                Layout.alignment: Qt.AlignVCenter
                                color: textColor
                                font.family: font1
                                font.pointSize: fontSize * 0.6 * (plasmoid.configuration.font1Size / 100);
                                text: weatherData.percentToDisplayString(weatherData.humidity)
                                renderType: Text.QtRendering
                                font.weight: Font.Medium
                            }
                        }
                    }
                }
            }
        }
        RowLayout {
            visible: weatherData.hasData
            Layout.topMargin: fontSize * -0.2
            spacing: fontSize / 4
            RowLayout {
                Layout.alignment: Qt.AlignLeft
                spacing: 0
                Item {
                    Layout.preferredWidth: fontSize * 1.2
                    Layout.preferredHeight: fontSize * 1.2
                    Layout.alignment: Qt.AlignVCenter
                    PlasmaCore.SvgItem {
                        id: heatIcon
                        visible: false
                        smooth: false
                        anchors.fill: parent
                        svg: PlasmaCore.Svg {imagePath: plasmoid.file("", "icons/" + "heat" + ".svg")}
                    }
                    ColorOverlay {
                        id: heatIconColor
                        anchors.fill: heatIcon
                        source:heatIcon
                        color: textColor
                    }
                }
                PlasmaComponents.Label {
                    readonly property var temp: weatherData.feels_like || weatherData.currentTemp
                    Layout.alignment: Qt.AlignVCenter
                    color: textColor
                    font.family: font1
                    font.pointSize: fontSize * 0.66 * (plasmoid.configuration.font1Size / 100);
                    text: weatherData.formatTemp(temp, true, false)
                    renderType: Text.QtRendering
                    font.weight: Font.Medium
                }
            }
            RowLayout {
                Layout.alignment: Qt.AlignLeft
                visible: weatherData.sunrise || weatherData.sunset
                spacing: 0
                Item {
                    Layout.preferredWidth: fontSize * 1.2
                    Layout.preferredHeight: fontSize * 1.2
                    Layout.alignment: Qt.AlignVCenter
                    PlasmaCore.SvgItem {
                        id: sunIcon
                        visible: false
                        smooth: false
                        anchors.fill: parent
                        svg: PlasmaCore.Svg {imagePath: plasmoid.file("", "icons/" + "horizon" + ".svg")}
                    }
                    ColorOverlay {
                        id: sunIconColor
                        anchors.fill: sunIcon
                        source:sunIcon
                        color: textColor
                    }
                }
                PlasmaComponents.Label {
                    Layout.alignment: Qt.AlignVCenter
                    property var tzOffset: (weatherData.tzOffset - dataSource.data.Local.Offset) * 1000
                    property date sunrise: {return new Date(weatherData.sunrise * 1000 + tzOffset)}
                    property date sunset: {return new Date(weatherData.sunset * 1000 + tzOffset)}
                    color: textColor
                    font.family: font1
                    font.pointSize: fontSize * 0.54 * (plasmoid.configuration.font1Size / 100);
                    text: `${Qt.formatDateTime(sunrise, "hh:mm")} - ${Qt.formatDateTime(sunset, "hh:mm")}`
                    renderType: Text.QtRendering
                    font.weight: Font.Medium
                }
            }
            RowLayout {
                Layout.alignment: Qt.AlignLeft
                visible: !!weatherData.windData
                spacing: 0
                Item {
                    Layout.preferredWidth: fontSize * 1.2
                    Layout.preferredHeight: fontSize * 1.2
                    Layout.alignment: Qt.AlignVCenter
                    PlasmaCore.SvgItem {
                        id: windIcon
                        visible: false
                        smooth: false
                        anchors.fill: parent
                        svg: PlasmaCore.Svg {imagePath: plasmoid.file("", "icons/" + "wind" + ".svg")}
                    }
                    ColorOverlay {
                        id: windIconColor
                        anchors.fill: windIcon
                        source:windIcon
                        color: textColor
                    }
                }
                PlasmaComponents.Label {
                    Layout.alignment: Qt.AlignVCenter
                    color: textColor
                    font.family: font1
                    font.pointSize: fontSize * 0.54 * (plasmoid.configuration.font1Size / 100);
                    text: weatherData.windSpeedText
                    renderType: Text.QtRendering
                    font.weight: Font.Medium
                }
            }
        }
        Rectangle {
            height: 1
            width: root.width * 0.8
            Layout.topMargin:5
        }
        Row {
            spacing: fontSize
            PlasmaComponents.Label {
                Layout.topMargin: fontSize / 2
                id: location
                color: textColor
                font.family: font2
                font.capitalization: Font.AllUppercase
                font.pointSize: fontSize * 1.68 * (plasmoid.configuration.font2Size / 100);
                Layout.maximumWidth: fontSize * 15
                wrapMode: Text.Wrap
                text: weatherData.hasData ? `${weatherData.stationCity}, ${weatherData.stationCountry}` : weatherData.country
                renderType: Text.QtRendering
            }
            PlasmaComponents.Label {
                Layout.topMargin: fontSize / 2
                anchors.verticalCenter: location.verticalCenter
                id: time
                color: hgColor
                font.family: font3
                font.pointSize: fontSize * 1.5 * (plasmoid.configuration.font3Size / 100);
                text: Qt.formatTime(dataSource.data.Local.DateTime, plasmoid.configuration.timeFormat);
                renderType: Text.QtRendering
                font.weight: Font.Black
            }
        }
    }
}
