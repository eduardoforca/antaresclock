// Version 8

// For testing the DataSource, enable the dataengine debug logging with:
// QT_LOGGING_RULES="kde.dataengine.weather=true" plasmoidviewer ...

import QtQuick 2.0
import QtQuick.Layouts 1.1
import org.kde.plasma.core 2.0 as PlasmaCore

import "../../code/weather_api.js" as WeatherAPI

import org.kde.plasma.private.weather 1.0 as WeatherPlugin

QtObject {
	property var data: undefined
	readonly property bool hasData: !!data
	property var locationData: ({})

	property QtObject displayUnits: DisplayUnits { id: displayUnits }

	readonly property string cityName: plasmoid.configuration.cityName
	readonly property string countryCode: plasmoid.configuration.countryCode || locationData["country code"] || ""
	readonly property string lang: {
		if(plasmoid.configuration.apiLang) {
			return plasmoid.configuration.apiLang
		} else {
			var loc = Qt.locale()
			if (loc.name == "pt_BR") {
				return loc.name
			} else {
				return loc.uiLanguages[0]
			}
		}
	}
	readonly property string apiKey: plasmoid.configuration.apiKey
	
	property var location: PlasmaCore.DataSource {
        id: location
        engine: "geolocation"
        connectedSources: ["location"]
        interval: plasmoid.configuration.updateInterval * 2000
		onNewData: {
			locationData = data
			timer.restart()
		}
    }

	readonly property string country: locationData.country || ""
	readonly property var locLat: locationData.latitude
	readonly property var locLon: locationData.longitude

	function updateCurrentData() {
        WeatherAPI.getCurrentData()
    }

	property var timer: Timer {
        interval: plasmoid.configuration.updateInterval * 1000
        repeat: true
        onTriggered: updateCurrentData()
		triggeredOnStart: true
    }
	
	function percentToDisplayString(percent) {
		if (typeof WeatherPlugin["Util"] !== "undefined") {
			// Plasma 5.13+
			return WeatherPlugin.Util.percentToDisplayString(percent)
		} else {
			// <= Plasma 5.12
			return percent + ' %'
		}
	}
	function valueToDisplayString(displayUnitType, value, valueUnitType, precision) {
		if (typeof WeatherPlugin["Util"] !== "undefined") {
			// Plasma 5.13+
			return WeatherPlugin.Util.valueToDisplayString(displayUnitType, value, valueUnitType, precision)
		} else {
			// <= Plasma 5.12
			return value.toFixed(precision)
		}
	}
	function temperatureToDisplayString(displayUnitType, value, valueUnitType, rounded, degreesOnly) {
		if (typeof WeatherPlugin["Util"] !== "undefined") {
			// Plasma 5.13+
			// rounded = typeof rounded === 'boolean' ? rounded : false
			// degreesOnly = typeof degreesOnly === 'boolean' ? degreesOnly : false
			return WeatherPlugin.Util.temperatureToDisplayString(displayUnitType, value, valueUnitType, rounded, degreesOnly)
		} else {
			// <= Plasma 5.12
			return Math.round(value) + '°'
		}
	}

	property string stationCity: {
		return (data && data["name"]) || cityName
	}

	property int tzOffset: {
		return (data && data["timezone"]) || 0
	}

	property string stationCountry: {
		if (data && data["sys"]) {
			return data["sys"]["country"] || countryCode
		}
		return countryCode
	}

	property string sunrise: {
		if (data && data["sys"]) {
			return data["sys"]["sunrise"] || ""
		}
		return ""
	}

	property string sunset: {
		if (data && data["sys"]) {
			return data["sys"]["sunset"] || ""
		}
		return ""
	}

	property var todayData: {
		if (data && data["weather"]) {
			return data["weather"][0]
		}
		return undefined
	}

	property string currentConditionIconName: {
		if (todayData && todayData["icon"]) {
			var timeChar = todayData["icon"].split(/\d+([dn]{1})/g)[1]
			return `${todayData["id"]}${timeChar}`
		}
		else{
			return "null"
		}
	}

	property string currentConditionsLong: {
		return (todayData && todayData["description"]) || ""
	}

	property string currentConditionsShort: {
		return (todayData && todayData["main"]) || ""
	}

	property var nowData: data ? data["main"] : undefined

	property var currentTemp: {
		return (!nowData || isNaN(nowData["temp"])) ? NaN : nowData["temp"]
	}

	property var maxTemp: {
		return (!nowData || isNaN(nowData["temp_max"])) ? NaN : nowData["temp_max"]
	}

	property var minTemp: {
		return (!nowData || isNaN(nowData["temp_min"])) ? NaN : nowData["temp_min"]
	}

	property var feelsLike: {
		return (!nowData || isNaN(nowData["feels_like"])) ? NaN : nowData["feels_like"]
	}

	property var humidity: {
		return (!nowData || isNaN(nowData["humidity"])) ? NaN : nowData["humidity"]
	}

	property var windData: data ? data["wind"] : undefined

	property var windSpeed: {
		return (!windData || isNaN(windData["speed"])) ? NaN : windData["speed"]
	}

	property var windAngle: {
		return (!windData || isNaN(windData["deg"])) ? NaN : windData["deg"]
	}

	property var windSpeedText: {
		var displaySpeedUnit = displayUnits.windSpeedUnitId
		if (windSpeed !== null && windSpeed !== "") {
			var windSpeedNumeric = (typeof windSpeed !== 'number') ? parseFloat(windSpeed) : windSpeed;
			if (!isNaN(windSpeedNumeric)) {
				if (windSpeedNumeric !== 0) {
					return valueToDisplayString(displaySpeedUnit, windSpeedNumeric, 9001, 1);
				} else {
					return i18ndc("plasma_applet_org.kde.plasma.weather", "Wind condition", "Calm")
				}
			} else {
				// TODO: i18n?
				return windSpeed;
			}
		}
	}

	function formatTemp(value, rounded, degreesOnly) {
		var displayUnitType = displayUnits.temperatureUnitId
		var text = temperatureToDisplayString(displayUnitType, value, 6000, rounded, degreesOnly)
		if (degreesOnly) {
			// Remove space between number and degree symbol
			text = text.replace(' ', '')
		}
		return text
	}
	function formatTempShort(value) {
		return formatTemp(value, true, true)
	}

	/**
	* Turn a 1-360° angle into the corresponding part on the compass.
	*
	* @param {number} deg Angle in degrees
	*
	* @returns {string} Cardinal direction
	*/
	function windDirToStr(deg) {
		var directions = [
			"N",
			"NNE",
			"NE",
			"ENE",
			"E",
			"ESE",
			"SE",
			"SSE",
			"S",
			"SSW",
			"SW",
			"WSW",
			"W",
			"WNW",
			"NW",
			"NNW",
		];
		deg *= 10;
		return directions[Math.round((deg % 3600) / 255)];
	}

	property var configConn: Connections {
		target: plasmoid.configuration
		function onApiKeyChanged() {
			timer.restart()
		}
		function onApiLangChanged() {
			timer.restart()
		}
		function onCityNameChanged() {
			timer.restart()
		}
		function onCountryCodeChanged() {
			timer.restart()
		}
	}
}
