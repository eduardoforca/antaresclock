# Alpaca Clock and Weather

https://store.kde.org/p/1666338

# About

Alpaca Clock and Weather is a Plasma Widget (Plasmoid) that shows date/time information as well as weather data in a pleasant manner. It is loosely based upon Antares - Conky Theme by Khiky-Merveilles.

# Installation

The easiest way to install the widget is through KDE, you just need to follow these steps:

1. Right Click Panel > Panel Options > Add Widgets
2. Get New Widgets > Download New Widgets
3. Search: Alpaca Clock and Weather
4. Install
5. Drag "Alpaca Clock and Weather" to your panel.

Otherwise, you can install it using your terminal and git:

- Clone this repository, and run the following commands inside the project folder

        kpackagetool5 -t Plasma/Applet -i plasmoid

You will be able to find this widget in the `Add Widget` Menu

# Set Up

Datetime info is taken from the system and weather data is taken from OpenWeatherMap API. In order to enable it, you must first create an API key on their website: https://openweathermap.org/api. The free plan is enough for the widget to function.

You can either input your location or let the plasmoid take it directly from your system.

If you want to change the API language, take a look at: https://openweathermap.org/current#multi

# Screenshots

![](https://images.pling.com/img/00/00/64/02/87/1666338/screenshot-20211216-165216.png)
![](https://images.pling.com/img/00/00/64/02/87/1666338/screenshot-20211216-170955.png)
![](https://images.pling.com/img/00/00/64/02/87/1666338/screenshot-20211216-1729371.png)
![](https://images.pling.com/img/00/00/64/02/87/1666338/screenshot-20211216-165125.png)
